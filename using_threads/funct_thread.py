import random
import sys
import time
from threading import Thread


def myFunct(name):
    for i in range(1, 50):
        time.sleep(random.random() * 0.1)
        sys.stdout.write(name)
        sys.stdout.flush()


if __name__ == '__main__':
    thr1 = Thread(target=myFunct, args=('1',))
    thr2 = Thread(target=myFunct, args=('2',))
    thr3 = Thread(target=myFunct, args=('3',))
    thr4 = Thread(target=myFunct, args=('4',))

    thr1.start()
    thr2.start()
    thr3.start()
    thr4.start()

    thr1.join()
    thr2.join()
    thr3.join()
    thr4.join()

