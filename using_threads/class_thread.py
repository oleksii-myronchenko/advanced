import random
import sys
import time
from threading import Thread


class SomeThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name

    def run(self) -> None:
        for i in range(1, 50):
            time.sleep(random.random() * 0.1)
            sys.stdout.write(self.name)
            sys.stdout.flush()


if __name__ == '__main__':
    thr1 = SomeThread('1')
    thr2 = SomeThread('2')
    thr3 = SomeThread('3')
    thr4 = SomeThread('4')

    thr1.start()
    thr2.start()
    thr3.start()
    thr4.start()

    thr1.join()
    thr2.join()
    thr3.join()
    thr4.join()


