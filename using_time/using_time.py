import time
from _tracemalloc import start
from functools import reduce
from timeit import default_timer


# we will use cProfile to profile something

def fib(n):
    if n <= 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def fib2(n):
    seq = (0, 1)
    for _ in range(2, n + 1):
        seq += (reduce(lambda a, b: a + b, seq[-2:]),)
    return seq[n]


if __name__ == '__main__':  # default timer is better because it is more cross platform
    fib(37)
    # start = default_timer()  # time.time()
    # print(fib(37))
    # end = default_timer()  # time.time()
    # print(f'It took {end - start} seconds')
    # start = default_timer()  # time.time()
    # print(fib2(37))
    # end = default_timer()  # time.time()
    # print(f'FIB2 took {end - start} seconds')
