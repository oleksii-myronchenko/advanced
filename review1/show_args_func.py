# we can declare our own custom decorators

# a decorator function
def show_args(f):
    def new_func(*args, **kwargs):
        # args is tuple
        # kwargs is dict
        print()
        print(f'We are running the function called  {f.__name__} ')
        print(f'docs are {f.__doc__}')
        print(f'Positional args are {args}')
        print(f'Keyword args are {kwargs}')
        return f(*args, **kwargs)

    return new_func

