from abc import ABCMeta, abstractmethod, abstractproperty


# declare an abstract class
class minMaxHolder:
    __metaclass__ = ABCMeta

    def __str__(self):
        return f'this class holds the min value of {self.min} and the max value of {self.max}'

    @property
    @abstractmethod
    def min(self):
        pass

    @property
    @abstractmethod
    def max(self):
        pass

