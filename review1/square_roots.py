from cmath import sqrt

from show_args_func import show_args
from abstract_square_roots import minMaxHolder
from review1.exceptions import SquareRootException
from functools import reduce


class SquareRoots(minMaxHolder):
    def __init__(self, min=0, max=100500):
        self.__min = min
        self.__max = max

    @property
    def min(self):
        return self.__min

    @property
    def max(self):
        return self.__max

    @show_args
    def sqrt(self, x):
        """
        sqrt
        :param x: x to sqrt
        :return: sqrt(x)
        """
        return sqrt(x)

    @show_args
    def __sqrt_starting_from_one_gen(self, n):
        """
        Returns generator with sqrt(x) where x is an int 0 < x < n
        :param n: n
        :return: generator with sqrt(x) where x is an int 0 < x <
        """
        return (self.sqrt(n) for n in range(1, n + 1))

    @show_args
    def __validate_input_number(self, n):
        """
        Validates an input string containing an input number (or a number itself), returning an int representation of an input
        :param n: the input number (string or int)
        :return: the int representation of n
        :raises SquareRootException is the input is not valid
        """
        try:
            int_n = int(n)
            if int_n > 0:
                if self.min < int_n < self.max:
                    return int_n
                else:
                    raise SquareRootException(
                        f'n should be between {self.min} and {self.max}, was {self.min} < {int_n} < {self.max}')
            else:
                raise SquareRootException(f'N should be a positive integer, was {int_n}')
        except Exception as e:
            raise SquareRootException(e)

    @show_args
    def print_sqrt_starting_from_one_to(self, n):
        """
        Prints all the square roots of ints from 0 to n
        :param n: n
        """
        int_n = self.__validate_input_number(n)

        g = self.__sqrt_starting_from_one_gen(int_n)
        for root in g:
            print(root)

    @show_args
    def save_to_file_sqrt_starting_from_one_to(self, n, filename):
        """
        Saves a square roots of numbers from 1 to n to the file
        :param n: n
        :param filename: file name
        """
        try:
            with open(filename, 'at') as file:
                int_n = self.__validate_input_number(n)

                g = self.__sqrt_starting_from_one_gen(int_n)
                for root in g:
                    print(root, file=file)
        except SquareRootException as e:
            raise e
        except Exception as e:
            raise SquareRootException(e)

    @show_args
    def tuple_of_sqrt_starting_from_one_to(self, n):
        """
        Creates a tuple of the square roots starting from 1 to n
        :param n: n
        :return: a tuple of the square roots starting from 1 to n
        """
        int_n = self.__validate_input_number(n)
        g = self.__sqrt_starting_from_one_gen(int_n)
        return tuple(g)

    @show_args
    def check_sqrt(self, n):
        """
        Checks whether the number is the square root of an int number
        :param n:
        :return:
        """
        root = sqrt(n)
        is_real_part_integer = root.real.is_integer()
        is_imaginary_part_integer = root.imag.is_integer()
        return is_imaginary_part_integer and is_real_part_integer

    def get_all_sqrts_in_min_max_range(self):
        """
        Gets all the int square roots in the range from min to max
        :return: all the square roots in the range from min to max
        """
        return tuple(filter(self.check_sqrt, map(self.sqrt, range(self.min, self.max))))


if __name__ == '__main__':
    sqroots = SquareRoots(-1001, 1001)
    sqroots.print_sqrt_starting_from_one_to(16)

    sqroots.save_to_file_sqrt_starting_from_one_to(4, 'roots.txt')

    print(sqroots.tuple_of_sqrt_starting_from_one_to(16))

    print(sqroots.check_sqrt(3))
    print(sqroots.check_sqrt(16))

    print(sqroots.check_sqrt(2j))

    print(sqroots.get_all_sqrts_in_min_max_range())
