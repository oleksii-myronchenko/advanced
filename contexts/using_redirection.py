# we may wish to send output to something othr than a console/terminal
# e.g. a log file
# or to a db, or other py module etc

import sys  # sys has stdout and stain


class Redirect:
    """
    Provide an easy way to redirect the standard output
    (which is the console by default)
    """

    def __init__(self, new_stdout):
        self.new_stdout = new_stdout

    def __enter__(self):  # overrides the built-in __enter__ method
        """
        We gonna implement a redirect for the output
        :return:
        """
        self.orig_stdout = sys.stdout  # make a record of the current stdout
        sys.stdout = self.new_stdout  # set a new stdout

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Restore the original stdout
        """
        sys.stdout = self.orig_stdout


if __name__ == '__main__':
    print(f'Currently, stdout is {sys.stdout} ')

    with open('syslog.txt', 'a') as stdout_file:
        with Redirect(stdout_file):
            print('this should b printed to the log file')
        print('this should b printed to the stdout')

        #create and destroy instances
        r = Redirect('ss')
        r = None
