# in Python 3 there is the context manager

from contextlib import contextmanager
import sys


@contextmanager
def stdout_redirect(new_stdout):
    old_stdout = sys.stdout
    sys.stdout = new_stdout
    yield  # this will yield the next object to be written
    sys.stdout = old_stdout  # return the stdout in place


if __name__ == '__main__':
    with open('syslog.txt', 'at') as syslog:
        with stdout_redirect(syslog):
            print('''
Something
That
Should
B
In A File
            ''')
    print('back to stdout')
