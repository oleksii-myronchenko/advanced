if __name__ == '__main__':
    # exploring zip (nothing to do with compression)
    days = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri')
    fruits = ['banana', 'orange', 'kiwi', 'durian']
    drink = ('coffee', 'tea', 'water', 'milk')
    after = ['tiramasu', 'ice cream', 'pie', 'creme caramel']

    # zip lets us combine data types together -- NOTHING TO DO WITH COMPRESSION
    j = zip(days, fruits, drink, after)

    print(j)

    for d, f, dr, a in j:  # zip stops when the shortest collection runs out of members
        print(f'On {d}, I ate {f} with {dr} and {a}')

    #exploring deque
    from collections import deque
    def pal(word):
        dq = deque(word)
        while len(dq) > 1:
            if dq.popleft() != dq.pop():
                return False
        return True

    print(pal("tenet"))
    print(pal("racecar"))
    print(pal("halibut"))
    print(pal("madam im adam"))
