# functional programming has returned oO
# as pure function has no side effects (entirely predictable)
# i.e. for given inputs the output is entirely determined
# Python provides the 'functools' library

from functools import reduce


def square(x):
    return x * x


def add(x, y):
    return x + y


def isOdd(a):
    """

    :param a: number
    :return:  true if odd, false otherwise
    """
    return a % 2 != 0


if __name__ == '__main__':
    # we may need a list of square numbers
    sq_l = list(map(square, range(1, 6)))
    print(sq_l)

    odd_l = list(filter(isOdd, range(-10, 10)))
    print(odd_l)

    r = reduce(add, odd_l, 10)  # sum every value from the list and add 10
    print(r)
