import sqlite3  # built in ppython
import constants

CREATE_SQL = f"""INSERT INTO {constants.TODO_TABLE}
                VALUES(?,?,?,?);"""

REFRESH_SQL = f"""SELECT * FROM {constants.TODO_TABLE}
                WHERE {constants.TODO_ID}=?;
"""

GET_ALL_SQL = f"""SELECT * FROM {constants.TODO_TABLE}"""

GET_ALL_FOR_USER_SQL = f"""SELECT * FROM {constants.TODO_TABLE}
                        WHERE {constants.TODO_USERID}=?;"""

GET_ALL_FOR_USERID_AND_COMPLETED_SQL=f"""SELECT * FROM {constants.TODO_TABLE}
                        WHERE {constants.TODO_USERID}=?
                        AND {constants.TODO_COMPLETED}=?;"""

UPDATE_SQL = f"""UPDATE {constants.TODO_TABLE}
            SET {constants.TODO_TITLE} = IFNull(?, {constants.TODO_TITLE}),
                {constants.TODO_COMPLETED} = IFNull(?, {constants.TODO_COMPLETED})
            WHERE {constants.TODO_ID} = ?;"""

DELETE_SQL = f"""DELETE FROM {constants.TODO_TABLE}
            WHERE {constants.TODO_ID}=?;"""


class TodoDAO:  # not rly a DAO but it should pass
    """
    Operates on dicts instead of rea objects
    """

    def __init__(self):
        self.conn = conn = sqlite3.connect(constants.DBNAME)
        #conn.set_trace_callback(print)

    def create_todo(self, id, userid, title, completed, connection=None):
        connection = self.__get_connection(connection)
        try:
            cursor = connection.cursor()
            connection.execute(CREATE_SQL, (id, userid, title, completed))
        except Exception as e:
            print(id, userid, title, completed)
            raise e
        finally:
            connection.commit()

    def get_todo(self, id, connection=None):
        connection = self.__get_connection(connection)
        try:
            cursor = connection.cursor()
            cursor.execute(REFRESH_SQL, tuple([id]))
            return cursor.fetchall()
        except Exception as e:
            raise e

    def get_completed_todos_for_userid(self, userid, connection=None):
        return self.__get_todos_for_userid_and_completed(userid, 'true', connection)

    def get_all_todos_for_userid(self, userid, connection=None):
        connection = self.__get_connection(connection)
        try:
            cursor = connection.cursor()
            cursor.execute(GET_ALL_FOR_USER_SQL, tuple([userid]))
            return cursor.fetchall()
        except Exception as e:
            raise e

    def __get_todos_for_userid_and_completed(self, userid, completed, connection=None):
        connection = self.__get_connection(connection)
        try:
            cursor = connection.cursor()
            cursor.execute(GET_ALL_FOR_USERID_AND_COMPLETED_SQL, (userid, completed))
            return cursor.fetchall()
        except Exception as e:
            raise e

    def get_all_todos(self, connection=None):
        connection = self.__get_connection(connection)
        try:
            cursor = connection.cursor()
            cursor.execute(GET_ALL_SQL)
            return cursor.fetchall()
        except Exception as e:
            raise e

    def update_todo(self, id, title=None, completed=None, connection=None):
        connection = self.__get_connection(connection)
        try:
            cursor = connection.cursor()
            cursor.execute(UPDATE_SQL, (title, completed, id))
            return cursor.fetchall()
        except Exception as e:
            raise e
        finally:
            connection.commit()

    def delete_todo(self, id, connection=None):
        connection = self.__get_connection(connection)
        try:
            connection.execute(DELETE_SQL, tuple([id]))
        except Exception as e:
            raise e
        finally:
            connection.commit()

    def __get_connection(self, connection):
        external_connection = connection is not None
        if not external_connection:
            connection = self.conn
        return connection


if __name__ == '__main__':
    tdao = TodoDAO()
    tdao.create_todo(1, 1, 'taiteru', 'false')
    print(tdao.get_todo(1))
    print(tdao.get_all_todos())
    tdao.update_todo(1,'TaItErU', 'true' )
    print(tdao.get_todo(1))
    tdao.update_todo(1, title='TAITERU')
    print(tdao.get_todo(1))
    tdao.update_todo(1, completed='false')
    print(tdao.get_todo(1))
    tdao.delete_todo(1)
    print(tdao.get_todo(1))
