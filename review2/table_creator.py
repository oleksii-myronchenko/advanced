import sqlite3  # built in ppython
import constants

if __name__ == '__main__':
    # make a connection
    conn = sqlite3.connect(constants.DBNAME)

    # we also need a cursor
    curs = conn.cursor()

    # let's store zoo animals
    # create name string up to 32 char
    # create count int
    # maintenance cost float

    # let's write a create stmt
    st = constants.TODO_DDL

    # execute the stmt
    curs.execute(st)

    conn.commit()
    conn.close()
