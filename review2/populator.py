import sqlite3

import req
import dao
from review2 import constants
from review2.dao import TodoDAO

if __name__ == '__main__':
    conn = sqlite3.connect(constants.DBNAME)
    dao = TodoDAO()

    data = req.getInfo().json()

    for item in data:
        dao.create_todo(item['id'], item['userId'], item['title'], str(item['completed']).lower(), conn)

    conn.commit()
    conn.close()
