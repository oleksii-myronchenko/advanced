# making use of the requests library
import json_sample

import requests  # may need to 'pip install request'


def getInfo():
    url = 'https://jsonplaceholder.typicode.com/todos'  # this API always returns JSON
    try:
        response = requests.get(url)
        data = json_sample.dumps(response.json(), indent=4)
        print(data)
        return response
    except Exception as err:
        print('Oops {}'.format(err))
    finally:
        pass


if __name__ == '__main__':
    getInfo()
