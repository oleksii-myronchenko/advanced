import json_sample

from review2.dao import TodoDAO


def main():
    dao = TodoDAO()
    valid = False
    while not valid:
        userId = input("Input the user ID>")
        user_id_int = int(float(userId))
        if user_id_int > 0:
            valid = True
        #print(json_sample.dumps(dao.get_all_todos(), indent=4))
        data = dao.get_completed_todos_for_userid(user_id_int)
        print(json_sample.dumps(data, indent=4))


if __name__ == '__main__':
    main()
