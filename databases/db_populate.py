import sqlite3


def main():
    # make a connection
    creatures = ({'creature': 'Albatros', 'count': 1, 'cost': 100500.0},
                 {'creature': 'Bear', 'count': 4, 'cost': 0.3},
                 {'creature': 'Carp', 'count': 400, 'cost': 1.0},
                 {'creature': 'Deer', 'count': 32, 'cost': 3.0},
                 {'creature': 'Eel', 'count': 9, 'cost': 1.0})
    conn = sqlite3.connect('my_db')
    curs = conn.cursor()
    for item in creatures:
        st = '''INSERT INTO zoo
            VALUES(?,?,?)'''  # rats are cheap
        try:
            curs.execute(st, (item['creature'], item['count'], item['cost']))
        except Exception as e:
            raise e
    try:
        conn.commit()
    except Exception as e:
        raise e
    finally:
        conn.close()


if __name__ == '__main__':
    main()
