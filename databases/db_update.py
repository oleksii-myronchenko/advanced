import sqlite3


def main():
    # make a connection
    conn = sqlite3.connect('my_db')
    invalid = True
    while invalid:
        c = input('Creature name>')
        if type(c) == str and len(c) > 0:
            invalid = False
        # ask what is the updated quantity
        invalid_quantity = True
        while invalid_quantity:
            q = input('How many>')
            if int(q) >= 0:
                invalid_quantity = False

    curs = conn.cursor()
    st = '''Update zoo set count = ?
    where creature = ?'''  # rats are cheap

    try:
        curs.execute(st, ( int(q), c))
        conn.commit()
    except Exception as e:
        raise e
    finally:
        conn.close()


if __name__ == '__main__':
    main()
