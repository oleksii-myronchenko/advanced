import sqlite3


def main():
    # make a connection
    conn = sqlite3.connect('my_db')
    invalid = True
    while invalid:
        c = input('Creature name>')
        if type(c) == str and len(c) > 0:
            invalid = False
        q = input('How many>')
        p = input('Cost>')

    curs = conn.cursor()
    st = '''INSERT INTO zoo
    VALUES(?,?,?)'''  # rats are cheap

    try:
        curs.execute(st, (c, q, p))
        conn.commit()
    except Exception as e:
        raise e
    finally:
        conn.close()


if __name__ == '__main__':
    main()
