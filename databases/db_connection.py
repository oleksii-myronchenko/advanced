import sqlite3  # built in ppython

# DB2
# import DB2
# conn = DB2.connect(dsn='ibm-DB', uid='analyst', pwd='db2pwd')

# Sybase
# import Sybase
# conn = Sybase.connect('SYBASE', 'username', 'passwd', 'databasename')

# Oracle
# import cx_Oracle
# conn = cx_Oracle.connect('username', 'passwd', 'hostname:port/SID')
# conn2 = cx_Oracle.connect('username/passwd@hostname:portno/SID')
# dsn_tns = cx_Oracle.makedsn('hostname', portno, 'SID')
# conn3 = cx_Oracle.connect('username', 'passwd', dsn_tns)

# MySQL
# import MySQLdb
# conn = MySQLdb.connect(host = "hostname", user = "username",
# passwd = "password", db = "dbname")

# PySQLite
# from pysqlite2 import dbapi2 as sqlite
# conn = sqlite.connect("mydb", connectionproperties)

# ODBC
# import odbc
# conn = odbc. odbc("myDSN/username/password")

if __name__ == '__main__':
    # make a connection
    conn = sqlite3.connect('my_db')

    # we also need a cursor
    curs = conn.cursor()

    # let's store zoo animals
    # create name string up to 32 char
    # create count int
    # maintenance cost float

    # let's write a create stmt
    st = '''CREATE TABLE zoo 
    (creature VARCHAR(32) PRIMARY KEY,
    count INT,
    cost FLOAT);
    '''

    # execute the stmt
    curs.execute(st)

    conn.commit()
    conn.close()
