# as well as text, we have bytes files.


if __name__ == "__main__":
    b = bytes(range(0, 256))  # 0--255
    print(b)

    # we'll try write and read bytews to external bytes
    with open('bfile', 'wb') as fout:  # write(overwrite) bytes
        fout.write(b)

    with open('bfile', 'rb') as fin:
        r = fin.read()
        print(r)