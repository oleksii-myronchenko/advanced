# file output and input
# python uses file access objects

def simple_output():
    fout = open("output.txt", 'a')  # a means append to file, w overrides, t means text, x means exclusive
    # x will raise Exception if a file exists
    print("here is the place that cat ate a rat",
          file=fout)  # NB print always adds a new line unless we have an end named variable
    fout.close()


def simple_input():
    try:
        fin = open("output.txt", "rt")  # r read, t text
        received = fin.read()  # reads the entire file
        print(received)
    except Exception as e:
        print(f"now some manure hits the climate control unit...: {e}")
    finally:
        fin.close()


# while works a bit like auto closeable (and try-except)
def file_writer(string='default'):
    try:
        fout = open('log.txt', 'xt')
        size = len(string)
        offset = 0
        chunk = 24
        while True:
            if offset > size:
                fout.write('\n')
                break
            else:
                fout.write(string[offset:offset + chunk])
                offset += chunk
    except FileExistsError as fee:
        print('The file already exists')
    except Exception as e:
        print(e)
    finally:
        print('all done')


if __name__ == "__main__":
    simple_output()
    simple_input()
    file_writer("LOOOOOOOOOOOOOOOOOOOOOONG TEEEEEEEEEEEEEEEEEEXT AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                "AAAAAAAAAAAAAAAAAAAAAAAAA")
