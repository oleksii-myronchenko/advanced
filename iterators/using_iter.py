# iter is built into Python
# it lets us iterate


if __name__ == '__main__':
    some_list = [False, 7, 'text', (5, 4, 3)]

    some_iter = iter(some_list)  # now we have an iterator

    print(some_iter.__next__())
    print(some_iter.__next__())
    print(some_iter.__next__())
    print(some_iter.__next__())

    r = reversed(some_list)

    print(r.__next__())
    print(r.__next__())
    print(r.__next__())
    print(r.__next__())