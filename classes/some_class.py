# class name has no bearing on the file na,e

class Duck():
    """
    Some Duck class used to xplain stuff
    """

    instanceCount = 0

    def __init__(self, the_only_friend):  # like a constructor but not a constuctor
        self.__name = 'Howard'
        self.__the_only_friend = the_only_friend
        Duck.instanceCount += 1  # count the instance

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @property
    def the_only_friend(self):
        return self.__the_only_friend

    def __str__(self):
        return f'This duck is called {self.__name} and its only friend is {self.__the_only_friend}'


if __name__ == '__main__':
    h = Duck('Ada')
    h.name = 'Howard the Duck'
    print(h.name, h.the_only_friend, h.__doc__)

    d = Duck('That cat that ate a rat')

    print(h)
    print(d)
    print(Duck.instanceCount)
