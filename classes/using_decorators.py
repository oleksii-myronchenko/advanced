# we can declare our own custom decorators

# a decorator function
def show_kwargs(f):
    def new_func(*args, **kwargs):
        # args is tuple
        # kwargs is dict
        print(f'We are running the function called  {f.__name__} ')
        print(f'Positional args are {args}')
        print(f'Keyword args are {kwargs}')
        return f(*args, **kwargs)

    return new_func


# a normal function
@show_kwargs  # use the decorator
def addInts(a, b):
    return a + b


if __name__ == '__main__':
    x, y = (1, 2)
    print(addInts(x, y))
