# there are thing built in Python
# many of them are intrinsic to Python

class TopLevel:
    def __init__(self):
        pass

    def __str__(self):
        return  'AAAAAAAAAA'


class Derived(TopLevel):
    """
    This class is derived from the TopLevel
    It has its own __str__ method
    """

    def ppp(self):
        print(super().__str__())

    def __init__(self):
        super().__init__()

    def __str__(self):
        return f'Derived class instance'


if __name__ == '__main__':
    t = TopLevel()
    d = Derived()

    print(d)
    # Let's explore the intrinsic members of the instances]
    print(f'class name is {Derived.__name__}')
    print(f'Class doc is {Derived.__doc__}')
    print(f'Class dict is {Derived.__dict__}')
    print(f'Class bases is {Derived.__bases__}')

