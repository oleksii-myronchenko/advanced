from my_abstract_class import Shape


class Circle(Shape):
    def __init__(self, name):
        self.name = name

    def display(self):
        print(f'Circle {self.name}')

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        if type(value) == str and value != '':
            self.__name = value
        else:
            self.__name = 'default'  # or raise an Exception why not


if __name__ == '__main__':
    c = Circle('Circle that ate a pigeon')
    c.display()
