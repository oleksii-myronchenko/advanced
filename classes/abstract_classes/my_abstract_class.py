from abc import ABCMeta, abstractmethod, abstractproperty


# declare an abstract class
class Shape:
    __metaclass__ = ABCMeta

    @abstractmethod
    def display(self):
        pass

    #    @abstractproperty # deprecated
    #    def name(self):
    #        pass

    @property
    @abstractmethod
    def name(self):
        pass
