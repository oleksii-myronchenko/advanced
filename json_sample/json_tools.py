import datetime
import json
import pickle

if __name__ == '__main__':
    # here is a rather complex data structure

    a = [{'name': 'PC', 'cost': 500, 'detail': {'a': 'True', 'b': [1, 2, 3, 4]}},
         {'name': 'Screen', 'cost': 250, 'detail': {'a': 'False', 'b': [9, 8, 7, 6]}}]

    print(json.dumps(a, indent=4))  # dumps renders json as plain text

    b = json.loads(json.dumps(a))

    print(a == b)

    # not everything can be serialized straight to json
    now = datetime.datetime.now()
    # now_j = json.dumps(now)  # will not work since the now is not serializable

    # we could use pickle
    now_p = pickle.dumps(now)
    print(now, type(now), now_p, type(now_p))

    c = pickle.loads(now_p)

    print(c == now)
