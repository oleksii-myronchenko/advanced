# we need a setr of classes that can encode our own class data type
import json
from typing import Any


class Item:
    """
    This class encapsulates items that have a name and a cost
    :param name: string name
    :param cost: float cost
    """
    def __init__(self, name, cost):
        self.name = name
        self. cost = cost
        pass

    def __str__(self):
        return f'Name {self.name}, Cost {self.cost:2f} '

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @property
    def cost(self):
        return self.__cost

    @cost.setter
    def cost(self, value):
        self.__cost = value


class ItemEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, Item):
            return o.__dict__
        else:
            return super().default(o)


if __name__ == '__main__':
    z = Item('Zoetrope', 0.12)
    z_j = json.dumps(z, cls=ItemEncoder)
    print(z)
    print(z_j)