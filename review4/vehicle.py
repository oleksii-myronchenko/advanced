from review4.swapi_base import SwapiBase


class Vehicle(SwapiBase):
    def __init__(self, data):
        super().__init__(data)

    def _get_data_for_file(self):
        return f'Vehicle: name {self.data["name"]}, model {self.data["model"]}, manufactured by {self.data["manufacturer"]}, \n'
    
    def __str__(self):
        return super().__str__()

    def __repr__(self):
        return super().__str__()
