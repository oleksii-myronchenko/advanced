from review4.swapi_base import SwapiBase


class Person(SwapiBase):
    def __init__(self, data):
        super().__init__(data)

    def _get_data_for_file(self):
        return f'Person: name {self.data["name"]}, Hair color is {self.data["hair_color"]}, height {self.data["height"]}, \n'

    def __str__(self):
        return super().__str__()

    def __repr__(self):
        return super().__str__()
