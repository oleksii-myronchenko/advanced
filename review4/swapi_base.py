import json
from abc import ABCMeta, abstractmethod, abstractproperty


class SwapiBase:
    __metaclass__ = ABCMeta

    def __init__(self, data):
        self.__data = data

    def append_to_file(self, filename):
        with open(filename, 'at') as file:
            file.write(self._get_data_for_file())
            file.flush()

    def __str__(self):
        return f'A Swapi {self.__class__.__name__}:\n {json.dumps(self.__data, indent=4)}'

    @property
    def data(self):
        return self.__data

    @abstractmethod
    def _get_data_for_file(self):
        pass
