from review4.swapi_base import SwapiBase


class Species(SwapiBase):
    def __init__(self, data):
        super().__init__(data)

    def _get_data_for_file(self):
        return f'Species: name {self.data["name"]}, designation: {self.data["designation"]}, homeworld link: {self.data["homeworld"]}, \n'
    def __str__(self):
        return super().__str__()

    def __repr__(self):
        return super().__str__()
