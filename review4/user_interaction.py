import swapi_service
from review4.swapi_exceptions import SwapiException


def check_exit(input):
    if input == 'exit':
        exit(0)


def main():
    category = input('Input category>>>')
    check_exit(category)

    id = input("Input id>>>")
    check_exit(id)

    swf = swapi_service.get_swapi(category, id)
    print(f'Swapi: {swf.swapi}')
    print(f'Films: {swf.films}')
    swf.swapi.append_to_file('swapi')


if __name__ == '__main__':
    while True:
        try:
            main()
        except SwapiException as se:
            print(f'We have an expected problem: {se}')
        except Exception as e:
            print(f'We have an UNEXPECTED problem: {e}')
