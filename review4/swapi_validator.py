from review4.swapi_exceptions import SwapiException

categories = ('planets', 'species', 'people', 'vehicles')


def validate_category(category):
    if not (category in categories):
        raise SwapiException(f'Category should be in {categories}')


def validate_id(id):
    return int(float(id))
