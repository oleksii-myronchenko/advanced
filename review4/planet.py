from review4.swapi_base import SwapiBase


class Planet(SwapiBase):
    def __init__(self, data):
        super().__init__(data)

    def _get_data_for_file(self):
        return f'Planet: name {self.data["name"]}, Orbital period is {self.data["orbital_period"]} days, population is {self.data["population"]} sentient lifeforms, \n'

    def __str__(self):
        return super().__str__()