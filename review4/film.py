from review4.swapi_base import SwapiBase


class Film(SwapiBase):
    def __init__(self, data):
        super().__init__(data)

    def _get_data_for_file(self):
        return 'Film'

    def __str__(self):
        return super().__str__()

    def __repr__(self):
        return f"\n{self.data['title']}"
