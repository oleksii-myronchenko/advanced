import concurrent.futures
import multiprocessing
from collections import namedtuple
from concurrent.futures import as_completed
from timeit import default_timer

import review4.swapi_factory as sf
from review4 import swapi_validator

swapi_with_films = namedtuple('SwapiWithFilms', ['swapi', 'films'])

exec = concurrent.futures.ThreadPoolExecutor(max_workers=multiprocessing.cpu_count())


def get_swapi(category, id):
    swapi_validator.validate_category(category)
    id_int = swapi_validator.validate_id(id)
    print('get_swapi started')
    start = default_timer()
    swapi = sf.get_swapi(category, id_int)
    swf = swapi_with_films(swapi, [])
    if len(swapi.data['films']) > 0:
        futures = []
        for film in swapi.data['films']:
            futures.append(exec.submit(sf.get_film_by_url, film))
        for future in as_completed(futures):
            swf.films.append(future.result())
    end = default_timer()
    print(f'The whole get_swapi took {end - start}s')
    return swf
