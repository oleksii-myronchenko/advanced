from timeit import default_timer

import requests

from review4.film import Film
from review4.planet import Planet
from review4.ppl import Person
from review4.species import Species
from review4.swapi_exceptions import SwapiException
from review4.vehicle import Vehicle


def get_swapi(category, id):
    data = __get_swapi_data_by_id_and_category(category, id)
    cat_l = category.lower()
    if cat_l == 'people':
        swapi = Person(data)
    elif cat_l == 'planets':
        swapi = Planet(data)
    elif cat_l == 'vehicles':
        swapi = Vehicle(data)
    elif cat_l == 'species':
        swapi = Species(data)
    elif cat_l == 'films':
        swapi = Film(data)
    else:
        raise SwapiException(f'Category {category} is not a valid category')
    return swapi


def __get_swapi_data_by_id_and_category(category, id):
    url = f'https://swapi.dev/api/{category}/{id}'
    return __get_swapi_data_by_url(url)


def __get_swapi_data_by_url(url):
    start = default_timer()
    response = requests.get(url)
    if not response.status_code == 200:
        raise SwapiException(f'Response code for the request to {url} was {response.status_code}, should be 200 ')
    # print(json.dumps(response.json(), indent=4))
    end = default_timer()
    print(f'Request to the URL {url} took {end - start}s')
    return response.json()


def get_film_by_url(url):
    if url.startswith("https://swapi.dev/api/films"):
        return Film(__get_swapi_data_by_url(url))
    else:
        raise SwapiException(f'The URL {url} could not be used for getting films.')


if __name__ == '__main__':
    print(__get_swapi_data_by_id_and_category('people', 1))
