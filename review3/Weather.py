import datetime
import json
import pickle
from typing import Any, Callable
import jsonpickle

import utils
from review3.country_code_utils import countryCodeUtils
from weather_exception import WeatherException


class Weather:
    """
    Weather class that holds the weather details for a single location
    THe location code is validates on creation using an external validator.
    """

    def __init__(self, country_code, city, temperature, humidity, wind_speed, description):
        self.country_code_validator = countryCodeUtils()
        if self.country_code_validator.validate_country_code(country_code):
            self.__country_code = country_code
        else:
            raise WeatherException(
                f'Country code {country_code} is not valid. Must be one of the {self.country_code_validator.country_codes}')
        self.__time = datetime.datetime.now()
        self.__description = utils.validate_string(description)
        self.__city = city
        self.__temperature = utils.validate_float(temperature)
        self.__humidity = utils.validate_float(humidity)
        self.__wind_speed = utils.validate_float(wind_speed)

    @property
    def country_code(self):
        return self.__country_code

    @property
    def city(self):
        return self.__city

    @property
    def temperature_kelvin(self):
        return self.__temperature + 273.15

    @property
    def temperature_fahrenheit(self):
        return (self.__temperature * 9 / 5) + 32

    @property
    def temperature(self):
        return self.__temperature

    @temperature.setter
    def temperature(self, value):
        self.__temperature = value

    @property
    def humidity(self):
        return self.__humidity

    @humidity.setter
    def humidity(self, value):
        self.__humidity = value

    @property
    def wind_speed(self):
        return self.__wind_speed

    @wind_speed.setter
    def wind_speed(self, value):
        self.__wind_speed = value

    @property
    def description(self):
        return self.__description

    @property
    def time(self):
        return self.__time

    def dump_to_file(self, file_name):
        with open(utils.validate_string(file_name), 'wb') as file:
            pickle.dump(self, file)

    def dump_description_to_file(self, file_name):
        with open(utils.validate_string(file_name), 'at') as file:
            file.writelines((self.__str__(), '\n'))

    def __getstate__(self):
        state = self.__dict__.copy()
        # Don't pickle validator
        del state["country_code_validator"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        # Add validator back since it doesn't exist in the pickle
        self.country_code_validator = countryCodeUtils()

    def __str__(self):
        return f"[{self.__time}]: The weather in {self.city}, {self.country_code_validator.country_name_by_code(self.country_code)} is as follows: temp {self.temperature:.2f} degrees C, {self.temperature_kelvin:2f} K, {self.temperature_fahrenheit:2f} F, wind speed {self.wind_speed} m/s, humidity {self.humidity} percent \n" \
               f"Conditions are: {self.description}"


class WeatherEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, Weather):
            return jsonpickle.encode(o)
        else:
            return super().default(o)


class WeatherDecoder(json.JSONDecoder):
    def decode(self, s: str, _w: Callable[..., Any] = ...) -> Any:
        return jsonpickle.decode(s)
