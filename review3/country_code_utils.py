from collections import namedtuple

import requests


class countryCodeUtils:
    def __init__(self):
        self.country_code_and_name_validation_results = namedtuple('ValidationResults', ['codeValid', 'nameValid'])
        self.country_code_and_name_validation_results.__new__.__defaults__ = (False, False)
        # self.__countries = tuple(requests.get('https://api.first.org/data/v1/countries').json()['data'].keys())
        self.__countries = requests.get('http://country.io/names.json').json()
        self.__country_codes = tuple(self.__countries.keys())

    @property
    def countries(self):
        return self.__countries

    @property
    def country_codes(self):
        return self.__country_codes

    def validate_country_code(self, country_code):
        return country_code in self.__country_codes

    def country_name_by_code(self, country_code):
        return self.countries[country_code]

    def country_code_by_name(self, country_name):
        for code, name in self.countries.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
            if name == country_name:
                return code
        return 'Narnia'

    def validate_country_by_code_or_name(self, country):
        if self.validate_country_code(country):
            return self.country_code_and_name_validation_results(True, False)
        if not self.country_code_by_name(country) == 'Narnia':
            return self.country_code_and_name_validation_results(False, True)
        return self.country_code_and_name_validation_results()


if __name__ == "__main__":
    countries = countryCodeUtils().countries
    print(countries)

    print(countryCodeUtils().validate_country_code('IE'))
    print(countryCodeUtils().validate_country_code('AZ'))
    print(countryCodeUtils().validate_country_code('GGG'))
    print(countryCodeUtils().country_name_by_code('UA'))

    response = requests.get('https://api.first.org/data/v1/countries')
    print(response.json()['data'].keys())
