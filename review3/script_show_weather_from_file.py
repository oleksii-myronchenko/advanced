import pickle
import sys
import Weather

from review3 import weather_factory
from review3.weather_exception import WeatherException

if __name__ == '__main__':
    if len(sys.argv) == 2:
        try:
            with open(sys.argv[1], 'rb') as file:
                w = pickle.load(file)
                print(f'Weather from file: {w}')

        except WeatherException as we:
            print(f"A problem occurred: {we}")
        except Exception as e:
            print(f'An UNEXPECTED problem occurred: {e}')

    else:
        print("The arguments are insufficient or too many. You should provide a file name. Example: script_show_weather_from_file.py filename")