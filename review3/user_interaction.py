import datetime

import weather_factory
from review3.weather_exception import WeatherException


def check_exit(s):
    if s == 'exit':
        exit(0)


def main():
    country = input('Enter the country name or code>>>')
    check_exit(country)
    city = input('Enter the city name>>>')
    check_exit(city)

    w = weather_factory.get_weather(city, country)
    print(w)
    w.dump_to_file(f'./files/{datetime.datetime.now()}_weather_in_{w.country_code}_{w.city}')
    w.dump_description_to_file('weather_hr.txt')


if __name__ == '__main__':
    while True:
        try:
            main()
        except WeatherException as we:
            print(f'We have a problem: {we}')
        except Exception as e:
            print(f'we have an UNEXPECTED problem: {e}')
