import sys

import weather_factory
from weather_exception import WeatherException

if __name__ == '__main__':
    if len(sys.argv) == 3:
        try:
            print(weather_factory.get_weather(sys.argv[2], sys.argv[1]))
        except WeatherException as we:
            print(f"A problem occurred: {we}")
        except Exception as e:
            print(f'An UNEXPECTED problem occurred: {e}')
    else:
        print("The arguments are insufficient or too many. You should provide a country code. Example: script_show_weather_by_city_and_country.py UA Odesa")