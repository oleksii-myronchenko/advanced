def validate_int(i):
    try:
        return int(float(i))
    except Exception as e:
        raise Exception(f'The value {i} must be an int. Cause: {e}')


def validate_float(i):
    try:
        return float(i)
    except Exception as e:
        raise Exception(f'The value {i} must be a float. Cause: {e}')


def validate_string(i):
    try:
        if type(i) == str:
            return i
        else:
            raise Exception(f'The value{i} should be a string. Cause: {e}')
    except Exception as e:
        raise Exception(f'The value {i} must be a string. Cause: {e}')
