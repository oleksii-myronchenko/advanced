import json

import requests
import country_code_utils
from review3.Weather import Weather, WeatherEncoder, WeatherDecoder
from review3.country_code_utils import countryCodeUtils
from review3.weather_exception import WeatherException


def get_weather(city, country):
    ccu = countryCodeUtils()
    vr = ccu.validate_country_by_code_or_name(country)
    if not vr.nameValid and not vr.codeValid:
        raise WeatherException(f'Country code/name {country} is invalid: {vr}')

    if vr.nameValid:
        country_name = country
    else:
        country_name = ccu.country_name_by_code(country)

    city_name = city  # todo: add validation

    json_weather = __get_weather_data(city_name, ccu.country_code_by_name(country_name))
    temp = json_weather['main']['temp']
    humidity = json_weather['main']['humidity']
    wind_speed = json_weather['wind']['speed']
    description = json_weather['weather'][0]['description']
    w = Weather(ccu.country_code_by_name(country_name), city_name, temp, humidity, wind_speed, description)
    return w


def __get_weather_data(city, country_code):
    url = f'http://api.openweathermap.org/data/2.5/weather?q={city.lower()},{country_code.lower()}&units=metric&APPID=48f2d5e18b0d2bc50519b58cce6409f1'
    response = requests.get(url)
    #print(json.dumps(response.json(), indent=4))
    return response.json()


if __name__ == '__main__':
    w = get_weather('Odesa', 'UA')
    print(w)

    print(json.dumps(w, cls=WeatherEncoder))

    print(json.loads(json.dumps(w, cls=WeatherEncoder), cls=WeatherDecoder))
