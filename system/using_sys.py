import sys  # we now have access to aspects of the platform we are running on

if __name__ == '__main__':
    print(sys.version, sys.version_info)
    print(sys.path)
    print(sys.platform)
    print(sys.base_prefix)

    print(sys.argv)
