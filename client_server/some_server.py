import socket


def someServer():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # now we configure a server
    param = ('localhost', 9874)
    server.bind(param)
    # begin listening for requests
    server.listen(10)
    print(f'the server is listening on port {param[1]} and host {param[0]}')
    # we need a run loop to respond to requests
    while True:
        # unpack the received request
        (client, addr) = server.accept()
        print(client, addr)
        buf = client.recv(1024)  # read the first 1024 bytes
        print(f'recvd: {buf}')
        # capitalize it and send it back
        client.send(buf.upper())
        # we can provide a way to quit the server
        if buf == b'quit':
            server.close()
            break


if __name__ == '__main__':
    someServer()
