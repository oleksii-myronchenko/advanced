import json

import requests


# make a POST request with some body
def makePost():
    url = 'https://httpbin.org/post'  # just an echo api
    payload = {'item': 'Cat that ate a rat', 'status': 'administrator'}  # eating rats gets you to places

    try:
        req = requests.post(url, data=payload)
        # print(req.text)
        print(json.dumps(req.json(), indent=4))
    except Exception as e:
        print(f'{e} is the cause of mayhem here')
        raise e


if __name__ == '__main__':
    makePost()
