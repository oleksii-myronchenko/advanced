import json

import requests
import sys


# we will access an API endpoint, passing params as sys argv
# parts ofthe returned json will be displayed nicely
def getInfo(category='todos', id=1):
    url = 'https://jsonplaceholder.typicode.com/'
    # append a category and is to it
    api = f'{url}{category}/{id}'  # this is a REST API
    try:
        response = requests.get(api)
        print(json.dumps(response.json(), indent=4))
    except Exception as e:
        print(f'Houston we have an {e}')
        raise e
    finally:
        pass


if __name__ == '__main__':
    if len(sys.argv) > 1:
        category = sys.argv[1]
        id = sys.argv[2]
    else:
        category = 'albums'
        id = 3
    getInfo(category, id)
