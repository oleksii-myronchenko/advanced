import socket
import sys


def someClient():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    param = ('localhost', 9874)
    sock.connect(param)
    # send some data
    sock.send('cat just ate a rat'.encode())  # uses the urlencode
    # handle the response from server
    res = sock.recv(1024)
    print(res)
    sock.close()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(param)
    if len(sys.argv) > 1:
        sock.send(' '.join(sys.argv[1:]).encode())

    res = sock.recv(1024)
    print(res)


if __name__ == '__main__':
    someClient()
