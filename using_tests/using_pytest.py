def test_thisWillPass():  # test prefix is MANDATORY
    assert ((1, 2, 3) == (1, 2, 3))


def test_thisWillFail():
    assert (1, 2, 3) == (3, 2, 1)


def test_testSets():
    assert {1, 2, 3} == {3, 2, 1}
