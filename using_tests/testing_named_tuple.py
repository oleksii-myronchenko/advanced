# invoke pytest
# python -m pytest testing_named_tuple

from collections import namedtuple

task = namedtuple('Task', ['SUmmary', 'Owner', 'done', 'id'])
# we can set defaults for values that may not be provided
task.__new__.__defaults__ = (None, None, False, None)


def test_defaults():
    """
    Test that the defaults for a named tuple work
    :return:
    """
    t = task()
    s = task(None, None, False, None)
    assert t == s


def test_member_access():
    """Check if we could access the members of the tuple using dot notation"""
    t = task('Eat a rat', 'Cat')
    assert t.SUmmary == 'Eat a rat'
    assert t.Owner == 'Cat'
    assert (t.done, t.id) == (False, None)
