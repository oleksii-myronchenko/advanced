from math import sqrt


class Point:
    points = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y
        Point.points += 1
        pass

    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, value):
        if type(value) == int:
            self.__x = value
        else:
            raise TypeError()

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self, value):
        if type(value) == int:
            self.__y = value
        else:
            raise TypeError()

    def hypot(self):
        """
        Derive the hypotenuse from x and y
        :return:
        """
        h = sqrt(self.x ** 2 + self.y ** 2)
        return h

    def moveBy(self, dx, dy):
        self.x += dx
        self.y += dy

    def __str__(self):
        return f'x: {self.x}, y: {self.y}'


if __name__ == '__main__':
    p1 = Point(3, 4)
    print(p1, p1.hypot())
    p2 = Point(-3, -4)
    print(p2)
    print(Point.point)
