import unittest

from some_point_class import Point


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.point = Point(3, 5)

    def test_moveby_a(self):
        # act
        self.point.moveBy(5, 2)
        # assert
        print(self.point)
        self.assertEqual(self.point.x, 8)
        self.assertEqual(self.point.y, 7)

    def test_moveby_a(self):
        # act
        self.point.moveBy(-5, -2)
        # assert
        print(self.point)
        self.assertEqual(self.point.x, -2)
        self.assertEqual(self.point.y, 3)

    def test_hypot(self):
        # arrange
        self.point.moveBy(1, -1)
        # act
        r = self.point.hypot()
        # assert
        self.assertAlmostEqual(r, 5.66, places=2)

    def test_point_counter(self):
        self.assertEqual(Point.point, 1)

    def test_exception_raised(self):
        """If x or y are non int we expect a type error"""
        with self.assertRaises(TypeError):
            Point('3', 4)
