import doctest


def nthPower(d, p):
    """
    This function returns d**p

    >>> nthPower(3, 2)
    9
    >>> nthPower(2, 3)
    8
    """
    return d ** p;


def cubeIt(a, b):
    """
    Cube it
    :return cubes for all the numbers from a to b

    >>> cubeIt(1,3)
    [1, 8, 27]

    >>> cubeIt(1,10) #doctest: +ELLIPSIS
    [1, 8, ..., 1000]
    """
    return [num ** 3 for num in range(a, b + 1)]


if __name__ == '__main__':
    doctest.testmod(verbose=True)
